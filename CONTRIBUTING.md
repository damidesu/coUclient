# Code of conduct

- Use open-source tools and libraries when possible
- Don't be evil

# Technical details

`coUclient` is based on [Dart](https://www.dartlang.org/), so the first thing you'll need to do (if you haven't already) is to install it.

## Project Layout

* `main.dart` serves as the main game loop. This class controls all functions within the game.
* Dart classes and functions are in the `lib/src` folder.
* Images, CSS and other web resources are in `web/assets`.
* More development documentation is in the `doc` folder.

## Setting up a development environment

1. [Install the Dart SDK](https://webdev.dartlang.org/tools/sdk#install) for your platform. Make sure you have at least version 2.0.0.
1. Make sure the SDK and your pub cache are on your path. On Linux, add this line to the end of your `~/.bashrc`: `export PATH=$PATH:"/usr/lib/dart/bin":"$HOME/.pub-cache/bin"
1. Install `webdev`: `pub global activate webdev`
1. Generate precompiled files: `webdev build`

### If you'd prefer an IDE

See https://www.dartlang.org/tools.

### Other platforms and/or manual installation

For instructions on manually installing Dart as well as links to other platforms, see https://www.dartlang.org/install .

## Building

### Command line

1. `pub get`
2. `webdev build`

## Running locally

The client uses the contents of `web/server_domain.txt` to find the server. If you're running the server locally,
it should contain `localhost`. For the values to use if you want to connect to the dev or live servers, please
contact someone on the development team.

1. `webdev serve`
