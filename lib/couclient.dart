library couclient;
/*
 *  THE CHILDREN OF UR WEBCLIENT
 *  http://www.childrenofur.com
*/

import 'dart:html';
import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'dart:js';
import 'dart:collection';
// (unused) // import 'dart:profiler';

// Libraries

import 'package:couclient/configs.dart'; // Global data
import 'package:couclient/src/network/server_interop/itemdef.dart'; // Items
import 'package:dnd/dnd.dart'; // Webaudio api
import 'package:gorgon/gorgon.dart'; // Audio and graphics
import 'package:intl/intl.dart'; // Used for NumberFormat
import 'package:json_annotation/json_annotation.dart';
import 'package:libld/libld.dart'; // Asset loading
import 'package:scproxy/scproxy.dart'; // SoundCloud helper
import 'package:transmit/transmit.dart'; // Event bus
import 'package:firebase/firebase.dart' as firebase; // Login
import 'package:angular/angular.dart';
import 'package:cou_login/cou_login/cou_login.template.dart' as loginComponent; // ignore: uri_has_not_been_generated

// Systems

part 'src/network/metabolics.dart'; // Metabolics display
part 'src/display/gps_display.dart';
part 'src/game/input.dart';
part 'src/game/joystick.dart';
part 'src/systems/clock.dart';
part 'src/systems/commands.dart';
part 'src/systems/gps.dart';
part 'src/systems/homestreet.dart';
part 'src/systems/quest_manager.dart';
part 'src/systems/util.dart';
part 'src/systems/weather.dart';

// Networking

part 'src/network/auth.dart';
part 'src/network/chat.dart';
part 'src/network/constants.dart';
part 'src/network/item_action.dart';
part 'src/network/metabolics_service.dart';
part 'src/network/server_interop/inventory.dart';
part 'src/network/server_interop/so_chat.dart';
part 'src/network/server_interop/so_item.dart';
part 'src/network/server_interop/so_multiplayer.dart';
part 'src/network/server_interop/so_player.dart';
part 'src/network/server_interop/so_street.dart';
part 'src/network/streetservice.dart';

// UI

part 'src/display/buff.dart';
part 'src/display/chatmessage.dart';
part 'src/display/chatpanel.dart';
part 'src/display/information_display.dart';
part 'src/display/inventory.dart';
part 'src/display/loop.dart';
part 'src/display/meters.dart';
part 'src/display/minimap.dart';
part 'src/display/render.dart';
part 'src/display/toast.dart';
part 'src/display/ui_templates/howmany.dart';
part 'src/display/ui_templates/interactions_menu.dart';
part 'src/display/ui_templates/item_chooser.dart';
part 'src/display/ui_templates/menu_keys.dart';
part 'src/display/ui_templates/right_click_menu.dart';
part 'src/display/view.dart';
part 'src/systems/audio.dart';
part "src/display/blog_notifier.dart";
part "src/display/darkui.dart";
part "src/display/overlays/text_anim.dart";

// Windows

part 'src/display/windows/achievements_window.dart';
part 'src/display/windows/add_friend_window.dart';
part 'src/display/windows/avatar_window.dart';
part 'src/display/windows/bag_window.dart';
part 'src/display/windows/bug_window.dart';
part 'src/display/windows/calendar_window.dart';
part 'src/display/windows/change_username_window.dart';
part 'src/display/windows/emoticon_picker.dart';
part 'src/display/windows/go_window.dart';
part 'src/display/windows/inv_search_window.dart';
part 'src/display/windows/item_window.dart';
part 'src/display/windows/mailbox_window.dart';
part 'src/display/windows/map_window.dart';
part 'src/display/windows/motd_window.dart';
part 'src/display/windows/note_window.dart';
part 'src/display/windows/quest_maker_window.dart';
part 'src/display/windows/questlog_window.dart';
part 'src/display/windows/prompt_str_window.dart';
part 'src/display/windows/rock_window.dart';
part 'src/display/windows/settings_window.dart';
part 'src/display/windows/shrine_window.dart';
part 'src/display/windows/useitem_window.dart';
part 'src/display/windows/vendor_window.dart';
part 'src/display/windows/weather_window.dart';
part 'src/display/windows/windows.dart';

// Overlays

part 'src/display/overlays/achievementget.dart';
part 'src/display/overlays/imgmenu.dart';
part 'src/display/overlays/levelup.dart';
part 'src/display/overlays/module_skills.dart';
part 'src/display/overlays/newdayscreen.dart';
part 'src/display/overlays/overlay.dart';
part 'src/display/overlays/streetloadingscreen.dart';

// Widgets

part 'src/display/widgets/soundcloud.dart';
part 'src/display/widgets/volumeslider.dart';

// Street rendering

part 'src/display/render/camera.dart';
part 'src/display/render/collision_lines_debug.dart';
part 'src/display/render/deco.dart';
part 'src/display/render/ladder.dart';
part 'src/display/render/platform.dart';
part 'src/display/render/signpost.dart';
part 'src/display/render/wall.dart';
part 'src/network/mapdata.dart';

// Game rendering

part 'src/game/action_bubble.dart';
part 'src/game/animation.dart';
part 'src/game/chat_bubble.dart';
part 'src/game/entities/door.dart';
part 'src/game/entities/entity.dart';
part 'src/game/entities/grounditem.dart';
part 'src/game/entities/npc.dart';
part 'src/game/entities/physics.dart';
part 'src/game/entities/plant.dart';
part 'src/game/entities/player.dart';
part 'src/game/entities/quoin.dart';
part 'src/game/entities/wormhole.dart';
part 'src/game/game.dart';
part 'src/game/street.dart';

// Built classes
part 'couclient.g.dart';

// Globals

final GpsIndicator gpsIndicator = new GpsIndicator(); // GPS status display
final NumberFormat commaFormatter = new NumberFormat("#,###"); // Comma format
final Random random = new Random(); // Random object
final Storage localStorage = window.localStorage; // Local storage
final Storage sessionStorage = window.sessionStorage; // Session storage
final String rsToken = "ud6He9TXcpyOEByE944g"; // Token for redstone

Constants constants; // Server-defined constants
MapData mapData; // Map, street, and hub metadata

AuthManager auth; // Auth server interop
CommandManager commandManager; // Command input
DateTime startTime; // Track times in log messages
Game game; // Game object
InputManager inputManager; // Input handler
MapWindow mapWindow; // Single-instance ma512-244-9024p window
Minimap minimap; // Minimap object
QuestManager questManager; // Quest manager
SoundManager audio; // Audio manager
WeatherManager weather; // Weather manager
WindowManager windowManager; // Window manager

bool get hasTouchSupport => context.callMethod("hasTouchSupport");

Future run() async {
	// Show the loading screen
	querySelector("#browser-error").hidden = true;
	querySelector("#loading").hidden = false;

	// Decide which UI to use
	checkMedia();

	// Start uptime counter
	startTime = new DateTime.now();

	// Gotta catch 'em all!
	startConsoleErrorLogging();

	try {
    // initialize firebase and the login component
    firebase.initializeApp(
        apiKey: 'AIzaSyCTXgszjO2AJNLTZUMYp2ZtFAmVLS2G6J4',
        authDomain: 'blinding-fire-920.firebaseapp.com',
    );
    runApp(loginComponent.CouLoginNgFactory);

		// Load server connection configuration
		await Configs.init();

		// Download the latest map data
		mapData = await MapData.download();

		// Make sure we have an up-to-date (1 day expiration) item cache
		await Item.loadItems();

		// Download constants
		constants = await Constants.download();
	} catch (e, st) {
		logmessage("Error loading server data: $e\n$st");
		serverDown = true;
	}

	try {
		// init ui
		view = new UserInterface();
		audio = new SoundManager();
		windowManager = new WindowManager();
		auth = new AuthManager();
		minimap = new Minimap();
		GPS.initWorldGraph();
		InvDragging.init();
	} catch (e, st) {
		logmessage("Error initializing interface: $e\n$st");
		new Toast(
			"OH NO! There was an error, so you should click here to reload."
			" If you see this several times, please file a bug report.",
			onClick: (_) => hardReload()
		);
	}

	// System
	new ClockManager();
	new CommandManager();

	// Watch for Collision-Triggered teleporters
	Wormhole.init();

	// Check the blog
	BlogNotifier.refresh();
}

// Clear cache with JS reload

void hardReload() {
	context["location"].callMethod("reload", [true]);
}

// Handle different device formats

enum ViewportMedia {
	DESKTOP,
	TABLET,
	MOBILE
}

void checkMedia() {
	// If the device is capable of touch events, assume the touch ui
	// unless the user has explicitly turned it off in the options.
	if (localStorage['interface'] == 'desktop') {
		// desktop already preferred
		setStyle(ViewportMedia.DESKTOP);
	} else if (localStorage['interface'] == 'mobile') {
		// mobile already preferred
		setStyle(ViewportMedia.MOBILE);
	} else if (hasTouchSupport) {
		// no preference, touch support, use mobile view
		setStyle(ViewportMedia.MOBILE);
		logmessage(
				"[Loader] Device has touch support, using mobile layout. "
						"Run /interface desktop in chat to use the desktop view."
		);
	} else if (!hasTouchSupport) {
		// no preference, no touch support, use desktop view
		setStyle(ViewportMedia.DESKTOP);
	}
}

void setStyle(ViewportMedia style) {
	/**
	 * The stylesheets are set up so that the desktop styles are always applied,
	 * the tablet styles are applied to tablets and phones, and the mobile style
	 * is only applied to phones:
	 *
	 * | Viewport | Desktop | Tablet  | Mobile  |
	 * |----------|---------|---------|---------|
	 * | Desktop  | Applied |         |         |
	 * | Tablet   | Applied | Applied |         |
	 * | Mobile   | Applied | Applied | Applied |
	 *
	 * Tablet provides touchscreen functionality and minimal optimization
	 * for a slightly smaller screen, while mobile prepares the UI
	 * for a very small viewport.
	 */

	LinkElement mobile = querySelector("#MobileStyle");
	LinkElement tablet = querySelector("#TabletStyle");

	switch (style) {
		case ViewportMedia.DESKTOP:
			mobile.disabled = true;
			tablet.disabled = true;
			break;

		case ViewportMedia.TABLET:
			mobile.disabled = true;
			tablet.disabled = false;
			break;

		case ViewportMedia.MOBILE:
			mobile.disabled = false;
			tablet.disabled = false;
			break;
	}

	if (style == ViewportMedia.TABLET || style == ViewportMedia.MOBILE) {
		querySelectorAll("html, body").onScroll.listen((Event e) {
			(e.target as Element).scrollLeft = 0;
			//print(e.target);
		});
	}
}
