import 'package:couclient/couclient.dart' as coUclient;

Future main() async {
  return coUclient.run();
}
